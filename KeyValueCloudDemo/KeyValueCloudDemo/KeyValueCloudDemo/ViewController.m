//
//  ViewController.m
//  KeyValueCloudDemo
//
//  Created by Darkwing Duck on 4/8/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "ViewController.h"
#import "KeyValueStorage.h"
#import <KSJSON.h>
#import <NativeiOSCloud/CloudKitStorage.h>
#import <NativeiOSCloud/CKRequestParamKeys.h>

@interface ViewController ()

@end

@implementation ViewController

CKDatabaseScope scope = CKDatabaseScopePrivate;
NSString *containerId = @"iCloud.com.blablagames.KeyValueCloudDemo";

NSString *recordId = @"record_5";
NSString *recordType = @"TestRecord";
//NSString *predicateFormat = @"name = 'John'";
NSString *predicateFormat = nil;
NSString *recordZone = @"TestRecords";
//NSString *recordZone = nil;

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (IBAction) saveRecordClicked: (id) sender
{
    NSMutableDictionary *request = [NSMutableDictionary dictionary];
    
    NSMutableDictionary *db = [NSMutableDictionary dictionary];
    [db setObject: containerId forKey: ContainerIdParamKey];
    [db setObject: [NSNumber numberWithInt: scope] forKey: DatabaseScopeParamKey];
    [request setObject: db forKey: DatabaseParamKey];
    
    NSMutableDictionary *record = [NSMutableDictionary dictionary];
    [record setObject: recordId forKey: RecordIdParamKey];
    [record setObject: recordType forKey: RecordTypeParamKey];
    
    if (recordZone)
    {
        [record setObject: recordZone forKey: ZoneIdParamKey];
    }
    
    [record setObject: @"John" forKey: @"name"];
    //    [record setObject: @"Smirnov" forKey: @"surname"];
    
    [request setObject: record forKey: RecordParamKey];
    
    NSString *serialized = [KSJSON serializeObject: request error: nil];
    NSLog(@"dict - %@", serialized);
    
    [[CloudKitStorage sharedInstance] saveRecord: serialized];
}

- (IBAction) removeRecordClicked: (id) sender
{
    NSMutableDictionary *request = [NSMutableDictionary dictionary];
    
    NSMutableDictionary *db = [NSMutableDictionary dictionary];
    [db setObject: containerId forKey: ContainerIdParamKey];
    [db setObject: [NSNumber numberWithInt: scope] forKey: DatabaseScopeParamKey];
    [request setObject: db forKey: DatabaseParamKey];
    
    NSMutableDictionary *record = [NSMutableDictionary dictionary];
    [record setObject: recordId forKey: RecordIdParamKey];
    
    if (recordZone)
    {
        [record setObject: recordZone forKey: ZoneIdParamKey];
    }
    
    [request setObject: record forKey: RecordParamKey];
    
    NSString *serialized = [KSJSON serializeObject: request error: nil];
    NSLog(@"dict - %@", serialized);
    
    [[CloudKitStorage sharedInstance] deleteRecord: serialized];
}

- (IBAction) fetchRecordClicked: (id) sender
{
    NSMutableDictionary *request = [NSMutableDictionary dictionary];
    
    NSMutableDictionary *db = [NSMutableDictionary dictionary];
    [db setObject: containerId forKey: ContainerIdParamKey];
    [db setObject: [NSNumber numberWithInt: scope] forKey: DatabaseScopeParamKey];
    [request setObject: db forKey: DatabaseParamKey];
    
    NSMutableDictionary *record = [NSMutableDictionary dictionary];
    [record setObject: recordId forKey: RecordIdParamKey];
    
    if (recordZone)
    {
        [record setObject: recordZone forKey: ZoneIdParamKey];
    }
    
    [request setObject: record forKey: RecordParamKey];
    
    NSString *serialized = [KSJSON serializeObject: request error: nil];
    NSLog(@"dict - %@", serialized);
    
    [[CloudKitStorage sharedInstance] fetchRecord: serialized];
}

- (IBAction) saveRecordZoneClicked: (id) sender
{
    NSMutableDictionary *request = [NSMutableDictionary dictionary];
    
    NSMutableDictionary *db = [NSMutableDictionary dictionary];
    [db setObject: containerId forKey: ContainerIdParamKey];
    [db setObject: [NSNumber numberWithInt: scope] forKey: DatabaseScopeParamKey];
    [request setObject: db forKey: DatabaseParamKey];
    
    NSMutableDictionary *zone = [NSMutableDictionary dictionary];
    [zone setObject: recordZone forKey: ZoneIdParamKey];
    [request setObject: zone forKey: ZoneParamKey];
    
    NSString *serialized = [KSJSON serializeObject: request error: nil];
    NSLog(@"dict - %@", serialized);
    
    [[CloudKitStorage sharedInstance] saveRecordZone: serialized];
}

- (IBAction) deleteZoneClicked: (id) sender
{
    NSMutableDictionary *request = [NSMutableDictionary dictionary];
    
    NSMutableDictionary *db = [NSMutableDictionary dictionary];
    [db setObject: containerId forKey: ContainerIdParamKey];
    [db setObject: [NSNumber numberWithInt: scope] forKey: DatabaseScopeParamKey];
    [request setObject: db forKey: DatabaseParamKey];
    
    NSMutableDictionary *zone = [NSMutableDictionary dictionary];
    [zone setObject: recordZone forKey: ZoneIdParamKey];
    [request setObject: zone forKey: ZoneParamKey];
    
    NSString *serialized = [KSJSON serializeObject: request error: nil];
    NSLog(@"dict - %@", serialized);
    
    [[CloudKitStorage sharedInstance] deleteRecordZone: serialized];
}

- (IBAction) fetchRecordZoneClicked: (id) sender
{
    NSMutableDictionary *request = [NSMutableDictionary dictionary];
    
    NSMutableDictionary *db = [NSMutableDictionary dictionary];
    [db setObject: containerId forKey: ContainerIdParamKey];
    [db setObject: [NSNumber numberWithInt: scope] forKey: DatabaseScopeParamKey];
    [request setObject: db forKey: DatabaseParamKey];
    
    NSMutableDictionary *zone = [NSMutableDictionary dictionary];
    [zone setObject: recordZone forKey: ZoneIdParamKey];
    [request setObject: zone forKey: ZoneParamKey];
    
    NSString *serialized = [KSJSON serializeObject: request error: nil];
    NSLog(@"dict - %@", serialized);
    
    [[CloudKitStorage sharedInstance] fetchRecordZone: serialized];
}

- (IBAction) fetchAllRecordZonesClicked: (id) sender
{
    NSMutableDictionary *request = [NSMutableDictionary dictionary];
    
    NSMutableDictionary *db = [NSMutableDictionary dictionary];
    [db setObject: containerId forKey: ContainerIdParamKey];
    [db setObject: [NSNumber numberWithInt: scope] forKey: DatabaseScopeParamKey];
    [request setObject: db forKey: DatabaseParamKey];
    
    NSString *serialized = [KSJSON serializeObject: request error: nil];
    NSLog(@"dict - %@", serialized);
    
    [[CloudKitStorage sharedInstance] fetchAllRecordZones: serialized];
}

- (IBAction) performQuery: (id) sender
{
    NSMutableDictionary *request = [NSMutableDictionary dictionary];
    
    NSMutableDictionary *db = [NSMutableDictionary dictionary];
    [db setObject: containerId forKey: ContainerIdParamKey];
    [db setObject: [NSNumber numberWithInt: scope] forKey: DatabaseScopeParamKey];
    [request setObject: db forKey: DatabaseParamKey];
    
    NSMutableDictionary *zone = [NSMutableDictionary dictionary];
    [zone setObject: recordZone forKey: ZoneIdParamKey];
    [request setObject: zone forKey: ZoneParamKey];
    
    NSMutableDictionary *query = [NSMutableDictionary dictionary];
    [query setObject: recordType forKey: RecordTypeParamKey];
    [request setObject: query forKey: QueryParamKey];
    
    if (predicateFormat)
    {
        NSMutableDictionary *predicate = [NSMutableDictionary dictionary];
        [predicate setObject: predicateFormat forKey: PredicateFormatParamKey];
        [query setObject: predicate forKey: PredicateParamKey];
    }
    
    NSString *serialized = [KSJSON serializeObject: request error: nil];
    NSLog(@"dict - %@", serialized);
    
    [[CloudKitStorage sharedInstance] performQuery: serialized];
}

- (IBAction) testFetchRecordsOperations: (id) sender
{
    NSMutableDictionary *request = [NSMutableDictionary dictionary];
    
    NSMutableDictionary *db = [NSMutableDictionary dictionary];
    [db setObject: containerId forKey: ContainerIdParamKey];
    [db setObject: [NSNumber numberWithInt: scope] forKey: DatabaseScopeParamKey];
    [request setObject: db forKey: DatabaseParamKey];
    
    NSMutableDictionary *operation = [NSMutableDictionary dictionary];
    [request setObject: operation forKey: OperationParamKey];
    
    // init list
    NSMutableArray *recordsList = [NSMutableArray array];
    
    NSMutableDictionary *record1 = [NSMutableDictionary dictionary];
    [record1 setObject: @"record_3" forKey: RecordIdParamKey];
    [record1 setObject: recordZone forKey: ZoneIdParamKey];
    [recordsList addObject: record1];
    
    NSMutableDictionary *record2 = [NSMutableDictionary dictionary];
    [record2 setObject: @"record_2" forKey: RecordIdParamKey];
    [record2 setObject: recordZone forKey: ZoneIdParamKey];
    [recordsList addObject: record2];
    //
    
    [operation setObject: recordsList forKey: RecordsListParamKey];
    
    NSString *serialized = [KSJSON serializeObject: request error: nil];
    NSLog(@"dict - %@", serialized);
    
    [[CloudKitStorage sharedInstance] fetchRecords: serialized];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
