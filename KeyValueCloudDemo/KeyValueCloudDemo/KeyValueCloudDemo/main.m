//
//  main.m
//  KeyValueCloudDemo
//
//  Created by Darkwing Duck on 4/8/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
