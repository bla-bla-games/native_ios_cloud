//
//  AppDelegate.h
//  KeyValueCloudDemo
//
//  Created by Darkwing Duck on 4/8/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

