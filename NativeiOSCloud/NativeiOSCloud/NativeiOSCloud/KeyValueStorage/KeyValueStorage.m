//
//  KeyValueStorage.m
//  NativeiOSCloud
//
//  Created by Sergey Smirnov on 4/8/17.
//  Support: d.duck.dev@gmail.com
//
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import <KSJSON/KSJSON.h>
#import <Utilities.m>
#import "KeyValueStorage.h"
#import <KVSUnityCallbacks.m>

@implementation KeyValueStorage

- (void) activate
{
    NSUbiquitousKeyValueStore *store = [NSUbiquitousKeyValueStore defaultStore];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(storageDidChange:)
                                                 name: NSUbiquitousKeyValueStoreDidChangeExternallyNotification
                                               object: store];
}

- (void) deactivate
{
    NSUbiquitousKeyValueStore *store = [NSUbiquitousKeyValueStore defaultStore];
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: NSUbiquitousKeyValueStoreDidChangeExternallyNotification
                                                  object: store];
}

#pragma mark - Set Methods

- (void) setString: (NSString *) value forKey: (nonnull NSString *) key
{
    NSLog(@"setString - key: %@, value: %@", key, value);
    [[NSUbiquitousKeyValueStore defaultStore] setString: value forKey: key];
}

- (void) setLong: (long long) value forKey: (nonnull NSString *) key
{
    [[NSUbiquitousKeyValueStore defaultStore] setLongLong: value forKey: key];
}

- (void) setDouble: (double) value forKey: (nonnull NSString *) key
{
    [[NSUbiquitousKeyValueStore defaultStore] setDouble: value forKey: key];
}

- (void) setBool: (bool) value forKey: (nonnull NSString *) key
{
    [[NSUbiquitousKeyValueStore defaultStore] setBool: value forKey: key];
}

#pragma mark - Get Methods

- (NSString *_Nullable) getStringForKey: (nonnull NSString *) key
{
    return [[NSUbiquitousKeyValueStore defaultStore] stringForKey: key];
}

- (long long) getLongForKey: (nonnull NSString *) key
{
    return [[NSUbiquitousKeyValueStore defaultStore] longLongForKey: key];
}

- (double) getDoubleForKey: (nonnull NSString *) key
{
    return [[NSUbiquitousKeyValueStore defaultStore] doubleForKey: key];
}

- (bool) getBoolForKey: (nonnull NSString *) key
{
    return [[NSUbiquitousKeyValueStore defaultStore] boolForKey: key];
}

- (void) removeValueForKey: (nonnull NSString *) key
{
    [[NSUbiquitousKeyValueStore defaultStore] removeObjectForKey: key];
}

- (bool) synchronize
{
    return [[NSUbiquitousKeyValueStore defaultStore] synchronize];
}

#pragma mark - Event Handlers

//
// This method is called when the key-value store in the cloud has changed externally.
//
- (void) storageDidChange: (NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSNumber *reasonForChange = [userInfo objectForKey: NSUbiquitousKeyValueStoreChangeReasonKey];
    
    if (![reasonForChange boolValue])
    {
        return;
    }
    
    NSDictionary *notificationObject = [self createNotification: userInfo];
    
    NSError *error;
    NSString *serialized = [KSJSON serializeObject: notificationObject error: &error];
    
    if (error == nil)
    {
        unityCallbacks.OnKVStorageChanged(fromNSString(serialized));
    }
    else
    {
        NSLog(@"Error while serializing notification: %@", error);
    }
}

- (NSDictionary *) createNotification: (NSDictionary *) userInfo
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    // get the reason for the notification (initial download, external change or quota violation change)
    NSInteger reason = [[userInfo objectForKey: NSUbiquitousKeyValueStoreChangeReasonKey] integerValue];
    
    NSArray *changedKeys = [userInfo objectForKey:NSUbiquitousKeyValueStoreChangedKeysKey];
    NSMutableArray *changedValues = [NSMutableArray array];
    
    // gather all changed values
    for (NSString *changedKey in changedKeys)
    {
        id value = [[NSUbiquitousKeyValueStore defaultStore] objectForKey: changedKey];
        
        NSDictionary<NSString *, id> *changePair = [NSDictionary dictionaryWithObjectsAndKeys:
                                                    changedKey, @"_key", value, @"_value", nil];
        [changedValues addObject: changePair];
    }
    
    [result setValue: [NSNumber numberWithInteger: reason] forKey: @"_reason"];
    [result setValue: changedValues forKey: @"_changedValues"];

    return result;
}

@end
