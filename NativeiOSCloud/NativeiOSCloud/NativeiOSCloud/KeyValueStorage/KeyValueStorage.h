//
//  KeyValueStorage.h
//  NativeiOSCloud
//
//  Created by Sergey Smirnov on 4/8/17.
//  Support: d.duck.dev@gmail.com
//
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeyValueStorage : NSObject

- (void) activate;
- (void) deactivate;

#pragma mark - Set Methods

- (void) setString: (nonnull NSString *) value forKey: (nonnull NSString *) key;
- (void) setLong: (long long) value forKey: (nonnull NSString *) key;
- (void) setDouble: (double) value forKey: (nonnull NSString *) key;
- (void) setBool: (bool) value forKey: (nonnull NSString *) key;

#pragma mark - Get Methods

- (NSString *_Nullable) getStringForKey: (nonnull NSString *) key;
- (long long) getLongForKey: (nonnull NSString *) key;
- (double) getDoubleForKey: (nonnull NSString *) key;
- (bool) getBoolForKey: (nonnull NSString *) key;

- (void) removeValueForKey: (nonnull NSString *) key;
- (bool) synchronize;

@end
