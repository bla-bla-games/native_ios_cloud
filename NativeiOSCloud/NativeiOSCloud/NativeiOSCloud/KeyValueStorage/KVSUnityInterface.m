//
//  KVStorageUnityInterface.m
//  NativeiOSCloud
//
//  Created by Sergey Smirnov on 4/9/17.
//  Support: d.duck.dev@gmail.com
//
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Utilities.m>
#import "KVSUnityCallbacks.m"
#import "NativeiOSCloud.h"

#pragma mark - General Methods

void _native_removeValue(const char *key)
{
    [[NativeiOSCloud sharedInstance].keyValue removeValueForKey: toNSString(key)];
}

bool _native_synchronize()
{
    return [[NativeiOSCloud sharedInstance].keyValue synchronize];
}

#pragma mark - Activation

void _native_activate(Unity_OnKVStorageChanged callback)
{
    unityCallbacks.OnKVStorageChanged = callback;
    [[NativeiOSCloud sharedInstance].keyValue activate];
}

void _native_deactivate()
{
    unityCallbacks.OnKVStorageChanged = NULL;
    [[NativeiOSCloud sharedInstance].keyValue deactivate];
}

#pragma mark - Set Methods

void _native_setString(const char *key, const char *value)
{
    [[NativeiOSCloud sharedInstance].keyValue setString: toNSString(value) forKey: toNSString(key)];
}

void _native_setLong(const char *key, long value)
{
    [[NativeiOSCloud sharedInstance].keyValue setLong: value forKey: toNSString(key)];
}

void _native_setDouble(const char *key, double value)
{
    [[NativeiOSCloud sharedInstance].keyValue setDouble: value forKey: toNSString(key)];
}

void _native_setBool(const char *key, bool value)
{
    [[NativeiOSCloud sharedInstance].keyValue setBool: value forKey: toNSString(key)];
}

#pragma mark - Get Methods

const char* _native_getString(const char *key)
{
    const char *result = fromNSString([[NativeiOSCloud sharedInstance].keyValue getStringForKey: toNSString(key)]);

    // new string allocation will free in managed code
    return copyString(result);
}

long long _native_getLong(const char *key)
{
    return [[NativeiOSCloud sharedInstance].keyValue getLongForKey: toNSString(key)];
}

double _native_getDouble(const char *key)
{
    return [[NativeiOSCloud sharedInstance].keyValue getDoubleForKey: toNSString(key)];
}

bool _native_getBool(const char *key)
{
    return [[NativeiOSCloud sharedInstance].keyValue getBoolForKey: toNSString(key)];
}
