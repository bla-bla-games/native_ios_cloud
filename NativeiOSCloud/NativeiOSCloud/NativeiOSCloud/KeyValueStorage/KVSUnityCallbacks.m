//
//  KVStorageUnityCallbacks.m
//  NativeiOSCloud
//
//  Created by Sergey Smirnov on 4/8/17.
//  Support: d.duck.dev@gmail.com
//
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (*Unity_OnKVStorageChanged)(const char *);

struct KVSUnityCallbacks
{
    Unity_OnKVStorageChanged OnKVStorageChanged;
} unityCallbacks;
