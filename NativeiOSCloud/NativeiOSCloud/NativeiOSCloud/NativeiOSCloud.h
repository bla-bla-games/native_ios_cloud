//
//  NativeiOSCloud.h
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 6/1/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CloudKitStorage.h"
#import "KeyValueStorage.h"

@interface NativeiOSCloud : NSObject

@property (nonatomic, readonly, strong) CloudKitStorage * _Nonnull cloudKit;
@property (nonatomic, readonly, strong) KeyValueStorage * _Nonnull keyValue;

+ (NativeiOSCloud *_Nonnull) sharedInstance;

@end
