//
//  Utilities.m
//  NativeiOSCloud
//
//  Created by Sergey Smirnov on 4/8/17.
//  Support: d.duck.dev@gmail.com
//
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import <Foundation/Foundation.h>

NSString* toNSString(const char *value)
{
    if (value == NULL)
    {
        return nil;
    }
    
    return [NSString stringWithUTF8String: value];
}

const char* fromNSString(NSString *value)
{
    if (value == nil)
    {
        return NULL;
    }
    
    return [value UTF8String];
}

char* copyString (const char* value)
{
    if (value == NULL)
        return NULL;
    
    char* result = (char*)malloc(strlen(value) + 1);
    strcpy(result, value);
    return result;
}
