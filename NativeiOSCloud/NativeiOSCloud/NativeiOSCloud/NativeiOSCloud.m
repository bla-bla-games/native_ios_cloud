//
//  NativeiOSCloud.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 6/1/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "NativeiOSCloud.h"

@implementation NativeiOSCloud

- (instancetype) init
{
    if (self = [super init])
    {
        _cloudKit = [[CloudKitStorage alloc] init];
        _keyValue = [[KeyValueStorage alloc] init];
    }
    
    return self;
}

#pragma mark - Singleton Instance

+ (NativeiOSCloud *) sharedInstance
{
    static NativeiOSCloud *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[NativeiOSCloud alloc] init];
    });
    
    return _sharedInstance;
}

@end
