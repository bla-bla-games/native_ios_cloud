//
//  CloudKitStorage.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/15/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CloudKitStorage.h"
#import <KSJSON.h>
#import "CKSSaveRecord.h"
#import "CKSSaveRecordZone.h"
#import "CKSDeleteRecord.h"
#import "CKSDeleteRecordZone.h"
#import "CKSFetchRecord.h"
#import "CKSFetchRecordZone.h"
#import "CKSFetchAllRecordZones.h"
#import "CKSPerformQuery.h"
#import "CKSFetchMultipleRecords.h"
#import "CKSFetchMultipleRecordZones.h"
#import "CKSModifyRecords.h"
#import "CKSModifyRecordZones.h"
#import "CKSCheckAccountStatus.h"

typedef enum
{
    iCloudAvailable = 0,
    iCloudAvailableWithDifferentAccount,
    iCloudNotAvailable
} iCloudAccountChangeReason;

@implementation CloudKitStorage

NSString * const DEFAULT_CONTAINER_ID = @"-1";
NSString * const UBIQUITY_IDENTITY_TOKEN_KEY = @"com.blablagames.cloudKitStorage.ubiquityIdentityToken";

- (instancetype) init
{
    if (self = [super init])
    {
        _activeRequests = [NSMutableDictionary dictionary];
    }
    
    return self;
}

- (void) activate
{
    [self initialize];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector (iCloudAccountAvailabilityChanged:) name: NSUbiquityIdentityDidChangeNotification object: nil];
}

- (void) initialize
{
    id currentiCloudToken = [[NSFileManager defaultManager] ubiquityIdentityToken];

    if (currentiCloudToken)
    {
        [self cacheiCloudToken: currentiCloudToken];
    }
    else
    {
        [self removeCachedToken];
    }
}

- (void) cacheiCloudToken: (id) token
{
    NSData *newTokenData = [NSKeyedArchiver archivedDataWithRootObject: token];
    [[NSUserDefaults standardUserDefaults] setObject: newTokenData forKey: UBIQUITY_IDENTITY_TOKEN_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) removeCachedToken
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey: UBIQUITY_IDENTITY_TOKEN_KEY];
}

- (void) iCloudAccountAvailabilityChanged: (NSNotification *) notification
{
    id currentiCloudToken = [[NSFileManager defaultManager] ubiquityIdentityToken];
    NSData *cachediCloudTokenData = (NSData *)[[NSUserDefaults standardUserDefaults] objectForKey: UBIQUITY_IDENTITY_TOKEN_KEY];
    id cachediCloudToken = cachediCloudTokenData ? [NSKeyedUnarchiver unarchiveObjectWithData: cachediCloudTokenData] : nil;
    
    if (!cachediCloudToken && !currentiCloudToken)
    {
        [self removeCachedToken];
        return;
    }
    
    if (!cachediCloudToken && currentiCloudToken)
    {
        // TODO: Send notification to C# that user logged in to iCloud
        //iCloudAccountChangeReason type = iCloudAvailable;
        [self cacheiCloudToken: currentiCloudToken];
        return;
    }
    
    if (cachediCloudToken && !currentiCloudToken)
    {
        //iCloudAccountChangeReason type = iCloudNotAvailable;
        // TODO: Send notification to C# that user logged out from iCloud
        return;
    }
    
    if (cachediCloudToken && currentiCloudToken)
    {
        if (![cachediCloudToken isEqual: currentiCloudToken])
        {
            //iCloudAccountChangeReason type = iCloudAvailableWithDifferentAccount;
            // TODO: Send notification to C# that user logged in to iCloud with different account
            [self cacheiCloudToken: currentiCloudToken];
            return;
        }
        else
        {
            //iCloudAccountChangeReason type = iCloudAvailable;
            // TODO: Send notification to C# that user logged in to iCloud
            [self cacheiCloudToken: currentiCloudToken];
        }
    }
}

- (void) deactivate
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey: UBIQUITY_IDENTITY_TOKEN_KEY];
    [[NSNotificationCenter defaultCenter] removeObserver: self name: NSUbiquityIdentityDidChangeNotification object: nil];
}

#pragma mark Account Status

- (void) accountStatus: (NSString *_Nonnull) requestData
{
    id requestObj = [KSJSON deserializeString: requestData error: nil];
    CKSCheckAccountStatus *request = [[CKSCheckAccountStatus alloc] initWithObject: requestObj andStorage: self];
    [self executeRequest: request];
}

#pragma mark Query

- (void) performQuery: (NSString *_Nonnull) requestData
{
    id requestObj = [KSJSON deserializeString: requestData error: nil];
    CKSPerformQuery *request = [[CKSPerformQuery alloc] initWithObject: requestObj andStorage: self];
    [self executeRequest: request];
}

#pragma mark Records

- (void) saveRecord: (NSString *_Nonnull) requestData
{
    id requestObj = [KSJSON deserializeString: requestData error: nil];
    CKSSaveRecord *request = [[CKSSaveRecord alloc] initWithObject: requestObj andStorage: self];
    [self executeRequest: request];
}

- (void) deleteRecord: (NSString *_Nonnull) requestData
{
    id requestObj = [KSJSON deserializeString: requestData error: nil];
    CKSDeleteRecord *request = [[CKSDeleteRecord alloc] initWithObject: requestObj andStorage: self];
    [self executeRequest: request];
}

- (void) fetchRecord: (NSString *_Nonnull) requestData
{
    id requestObj = [KSJSON deserializeString: requestData error: nil];
    CKSFetchRecord *request = [[CKSFetchRecord alloc] initWithObject: requestObj andStorage: self];
    [self executeRequest: request];
}

#pragma mark Zones

- (void) saveRecordZone: (NSString *_Nonnull) requestData
{
    id requestObj = [KSJSON deserializeString: requestData error: nil];
    CKSSaveRecordZone *request = [[CKSSaveRecordZone alloc] initWithObject: requestObj andStorage: self];
    [self executeRequest: request];
}

- (void) deleteRecordZone: (NSString *_Nonnull) requestData
{
    id requestObj = [KSJSON deserializeString: requestData error: nil];
    CKSDeleteRecordZone *request = [[CKSDeleteRecordZone alloc] initWithObject: requestObj andStorage: self];
    [self executeRequest: request];
}

- (void) fetchRecordZone: (NSString *_Nonnull) requestData
{
    id requestObj = [KSJSON deserializeString: requestData error: nil];
    CKSFetchRecordZone *request = [[CKSFetchRecordZone alloc] initWithObject: requestObj andStorage: self];
    [self executeRequest: request];
}

- (void) fetchAllRecordZones: (NSString *_Nonnull) requestData
{
    id requestObj = [KSJSON deserializeString: requestData error: nil];
    CKSFetchAllRecordZones *request = [[CKSFetchAllRecordZones alloc] initWithObject: requestObj andStorage: self];
    [self executeRequest: request];
}

#pragma mark Operations

- (void) fetchMultipleRecords: (NSString *_Nonnull) requestData
{
    id requestObj = [KSJSON deserializeString: requestData error: nil];
    CKSFetchMultipleRecords *request = [[CKSFetchMultipleRecords alloc] initWithObject: requestObj andStorage: self];
    [self executeRequest: request];
}

- (void) fetchMultipleRecordZones: (NSString *_Nonnull) requestData
{
    id requestObj = [KSJSON deserializeString: requestData error: nil];
    CKSFetchMultipleRecordZones *request = [[CKSFetchMultipleRecordZones alloc] initWithObject: requestObj andStorage: self];
    [self executeRequest: request];
}

- (void) modifyRecords: (NSString *_Nonnull) requestData
{
    id requestObj = [KSJSON deserializeString: requestData error: nil];
    CKSModifyRecords *request = [[CKSModifyRecords alloc] initWithObject: requestObj andStorage: self];
    [self executeRequest: request];
}

- (void) modifyRecordZones: (NSString *_Nonnull) requestData
{
    id requestObj = [KSJSON deserializeString: requestData error: nil];
    CKSModifyRecordZones *request = [[CKSModifyRecordZones alloc] initWithObject: requestObj andStorage: self];
    [self executeRequest: request];
}

#pragma mark CKSRequestDelegate

- (void) requestDidFinish: (CKSRequest *) request
{
    [self finalizeRequest: request];
}

#pragma mark Utils

- (void) executeRequest: (CKSRequest *) request
{
    request.delegate = self;
    [self.activeRequests setObject: request forKey: request.requestId];
    [request execute];
}

- (void) finalizeRequest: (CKSRequest *) request
{
    [request finalize];
    [self.activeRequests removeObjectForKey: request.requestId];
}

- (CKContainer *) getContainer: (NSString *_Nullable) containerId
{
    if (!containerId || [containerId isEqualToString: DEFAULT_CONTAINER_ID])
    {
        return [CKContainer defaultContainer];
    }

    return [CKContainer containerWithIdentifier: containerId];
}

- (CKDatabase *) getDatabase: (CKContainer *) container withDBScope: (CKDatabaseScope) scope
{
    switch (scope)
    {
        case CKDatabaseScopePublic:
            return container.publicCloudDatabase;
        case CKDatabaseScopePrivate:
            return container.privateCloudDatabase;
        case CKDatabaseScopeShared:
            return container.sharedCloudDatabase;
            
        default:
            return container.privateCloudDatabase;
    }
}

@end
