//
//  CloudKitUnitySerializer.h
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/28/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CloudKit/CloudKit.h>

@interface CKSUnitySerializer : NSObject

+ (id) serializeRecord: (CKRecord *) record;
+ (CKRecord *) deserializeRecord: (id) recordData;

+ (id) serializeRecordID: (CKRecordID *) recordID;
+ (CKRecordID *) deserializeRecordID: (id) recordIDData;

+ (id) serializeZone: (CKRecordZone *) zone;
+ (CKRecordZone *) deserializeZone: (id) zoneData;

+ (id) serializeZoneID: (CKRecordZoneID *) zoneID;
+ (CKRecordZoneID *) deserializeZoneID: (id) zoneIDData;

+ (id) serializeError: (NSError *) error;
+ (id) serializeRecordError: (CKRecordID *) recordId error: (NSError *) error;
+ (id) serializeRecordZoneError: (NSString *) zoneName error: (NSError *) error;

+ (NSString *) parseZoneName: (NSString *) zoneName;
+ (NSString *) getZoneName: (CKRecordZoneID *) zoneId;
+ (BOOL) isDefaultZoneId: (CKRecordZoneID *) zoneId;

@end
