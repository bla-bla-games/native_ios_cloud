//
//  CKCommand.h
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/19/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CloudKitStorage.h"
#import <CKSUnityCallbacks.m>
#import "CKSUnitySerializer.h"
#import "CKSRequestDelegate.h"
#import "CKSRequestParamKeys.h"

@interface CKSRequest : NSObject

@property (nonatomic, readwrite, weak) id<CKSRequestDelegate> delegate;
@property (nonatomic, readonly, strong) NSString *requestId;
@property (nonatomic, readonly, weak) CloudKitStorage *storage;

- (instancetype) initWithObject: (id) obj andStorage: (CloudKitStorage *) storage;
- (void) execute;
- (void) processResponse: (NSString *) requestId data: (NSDictionary *) responseData;
- (void) sendResult: (const char *) requestId responseData: (const char *) responseData;
- (void) callInMainThread: (void (^)(void)) blockToCall;
- (void) finalize;

@end
