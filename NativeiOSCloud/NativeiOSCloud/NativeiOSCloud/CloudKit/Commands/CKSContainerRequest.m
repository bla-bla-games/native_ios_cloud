//
//  CKSContainerRequest.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 6/14/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSContainerRequest.h"

@implementation CKSContainerRequest

- (instancetype) initWithObject: (id) obj andStorage: (CloudKitStorage *) storage
{
    if (self = [super initWithObject: obj andStorage: storage])
    {
        [self initContainer: obj];
    }
    
    return self;
}

- (void) initContainer: (id) data
{
    NSString *containerId = [data objectForKey: ContainerIdParamKey];
    _container = [self.storage getContainer: containerId];
}

@end
