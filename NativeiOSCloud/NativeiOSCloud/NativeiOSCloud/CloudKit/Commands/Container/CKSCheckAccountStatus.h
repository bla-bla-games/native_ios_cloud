//
//  CKSAccountStatus.h
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 6/14/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSRequest.h"
#import "CKSContainerRequest.h"

@interface CKSCheckAccountStatus : CKSContainerRequest

@end
