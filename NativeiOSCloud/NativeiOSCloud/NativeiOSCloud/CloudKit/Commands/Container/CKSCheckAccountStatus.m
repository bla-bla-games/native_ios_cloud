//
//  CKSAccountStatus.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 6/14/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSCheckAccountStatus.h"

@implementation CKSCheckAccountStatus

- (void) execute
{
    __weak typeof(self) weakSelf = self;
    
    [self.container accountStatusWithCompletionHandler: ^(CKAccountStatus accountStatus, NSError *error)
    {
        typeof(self) strongSelf = weakSelf;
        if (!strongSelf) return;
         
        NSMutableDictionary *result = [NSMutableDictionary dictionary];
        BOOL isSuccess = YES;
         
        if (error)
        {
            NSDictionary *errorDict = [CKSUnitySerializer serializeError: error];
            [result setObject: errorDict forKey: ErrorParamKey];
            isSuccess = NO;
        }

        [result setObject: [NSNumber numberWithInteger: accountStatus] forKey: AccountStatusParamKey];
        [result setObject: [NSNumber numberWithBool: isSuccess] forKey: IsSuccessParamKey];
        
        [strongSelf processResponse: strongSelf.requestId data: result];
    }];
}

- (void) sendResult: (const char *) requestId responseData: (const char *) responseData
{
    unityCallbacks.HandleAccountStatus(requestId, responseData);
}

@end
