//
//  SaveZoneCommand.h
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/20/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CloudKit/CloudKit.h>
#import "CKSRequest.h"
#import "CKSDatabaseRequest.h"

@interface CKSSaveRecordZone : CKSDatabaseRequest

@property (nonatomic, readonly, strong) CKRecordZone *zone;

@end
