//
//  CKSaveCommand.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/17/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSSaveRecord.h"

@implementation CKSSaveRecord

- (instancetype) initWithObject: (id) obj andStorage: (CloudKitStorage *) storage
{
    if (self = [super initWithObject: obj andStorage: storage])
    {
        id recordData = [obj objectForKey: RecordParamKey];
        [self initRecord: recordData];
    }
    
    return self;
}

- (void) initRecord: (id) data
{
    _record = [CKSUnitySerializer deserializeRecord: data];
}

- (void) execute
{
    __weak typeof(self) weakSelf = self;
    
    [self.database saveRecord: self.record completionHandler: ^(CKRecord *record, NSError *error)
    {
        typeof(self) strongSelf = weakSelf;
        if (!strongSelf) return;
        
        NSMutableDictionary *result = [NSMutableDictionary dictionary];
        BOOL isSuccess = YES;
        
        if (error)
        {
            NSDictionary *errorDict = [CKSUnitySerializer serializeError: error];
            [result setObject: errorDict forKey: ErrorParamKey];
            isSuccess = NO;
        }
        
        if (record)
        {
            NSDictionary *recordDict = [CKSUnitySerializer serializeRecord: record];
            [result setObject: recordDict forKey: RecordParamKey];
        }
        
        [result setObject: [NSNumber numberWithBool: isSuccess] forKey: IsSuccessParamKey];
        [strongSelf processResponse: strongSelf.requestId data: result];
    }];
}

- (void) sendResult: (const char *) requestId responseData: (const char *) responseData
{
    unityCallbacks.HandleSaveRecord(requestId, responseData);
}

@end
