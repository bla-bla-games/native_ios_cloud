//
//  CKFetchRecordZoneCommand.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/21/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSFetchRecordZone.h"

@implementation CKSFetchRecordZone

- (instancetype) initWithObject: (id) obj andStorage: (CloudKitStorage *) storage
{
    if (self = [super initWithObject: obj andStorage: storage])
    {
        [self initZoneId: obj];
    }
    
    return self;
}

- (void) initZoneId: (id) data
{
    NSString *zoneName = [data objectForKey: ZoneNameParamKey];
    _zoneId = [[CKRecordZoneID alloc] initWithZoneName: zoneName ownerName: CKCurrentUserDefaultName];
}

- (void) execute
{
    __weak typeof(self) weakSelf = self;
    
    [self.database fetchRecordZoneWithID: self.zoneId completionHandler: ^(CKRecordZone *zone, NSError *error)
    {
        typeof(self) strongSelf = weakSelf;
        if (!strongSelf) return;
        
        NSMutableDictionary *result = [NSMutableDictionary dictionary];
        BOOL isSuccess = YES;
        
        if (error)
        {
            NSDictionary *errorDict = [CKSUnitySerializer serializeError: error];
            [result setObject: errorDict forKey: ErrorParamKey];
            isSuccess = NO;
        }
        
        if (zone)
        {
            NSDictionary *zoneDict = [CKSUnitySerializer serializeZone: zone];
            [result setObject: zoneDict forKey: ZoneParamKey];
        }
        
        [result setObject: [NSNumber numberWithBool: isSuccess] forKey: IsSuccessParamKey];
        [strongSelf processResponse: strongSelf.requestId data: result];
    }];
}

- (void) sendResult: (const char *) requestId responseData: (const char *) responseData
{
    unityCallbacks.HandleFetchRecordZone(requestId, responseData);
}

@end
