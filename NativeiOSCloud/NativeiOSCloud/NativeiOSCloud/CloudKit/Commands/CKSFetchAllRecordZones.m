//
//  CKFetchAllRecordZonesCommand.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/21/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSFetchAllRecordZones.h"

@implementation CKSFetchAllRecordZones

- (void) execute
{
    __weak typeof(self) weakSelf = self;
    
    [self.database fetchAllRecordZonesWithCompletionHandler: ^(NSArray<CKRecordZone *> *zones, NSError *error)
    {
        typeof(self) strongSelf = weakSelf;
        if (!strongSelf) return;
        
        NSMutableDictionary *result = [NSMutableDictionary dictionary];
        BOOL isSuccess = YES;
        
        if (error)
        {
            NSDictionary *errorDict = [CKSUnitySerializer serializeError: error];
            [result setObject: errorDict forKey: ErrorParamKey];
            isSuccess = NO;
        }
        
        if (zones)
        {
            NSMutableArray *zonesList = [NSMutableArray array];
            
            for (CKRecordZone *zone in zones)
            {
                NSDictionary *zoneDict = [CKSUnitySerializer serializeZone: zone];
                [zonesList addObject: zoneDict];
            }
            
            [result setObject: zonesList forKey: ZonesListParamKey];
        }
        
        [result setObject: [NSNumber numberWithBool: isSuccess] forKey: IsSuccessParamKey];
        [strongSelf processResponse: strongSelf.requestId data: result];
    }];
}

- (void) sendResult: (const char *) requestId responseData: (const char *) responseData
{
    unityCallbacks.HandleFetchAllRecordZones(requestId, responseData);
}

@end
