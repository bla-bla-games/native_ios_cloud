//
//  CKRequestParamKeys.h
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/20/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import <Foundation/Foundation.h>

NSString * const RequestIdParamKey = @"_id";
NSString * const DatabaseParamKey = @"_database";
NSString * const RecordParamKey = @"_record";
NSString * const ContainerIdParamKey = @"_containerId";
NSString * const DatabaseScopeParamKey = @"_scope";
NSString * const RecordIdParamKey = @"_recordId";
NSString * const RecordTypeParamKey = @"_recordType";
NSString * const ZoneParamKey = @"_zone";
NSString * const ZonesListParamKey = @"_zones";
NSString * const ZoneIdParamKey = @"_zoneId";
NSString * const ZoneOwnerParamKey = @"_zoneOwner";
NSString * const QueryParamKey = @"_query";
NSString * const PredicateParamKey = @"_predicate";
NSString * const PredicateFormatParamKey = @"_predicateFormat";
NSString * const OperationParamKey = @"_operation";
NSString * const RecordsListParamKey = @"_records";
NSString * const RecordFieldsListParamKey = @"_fields";
NSString * const RecordFieldKeyParamKey = @"_key";
NSString * const RecordFieldValueParamKey = @"_value";
NSString * const IsSuccessParamKey = @"_isSuccess";
NSString * const RecordsErrorsParamKey = @"_recordErrors";
NSString * const RecordZonesErrorsParamKey = @"_zonesErrors";
NSString * const RecordNameParamKey = @"_recordName";
NSString * const ZoneNameParamKey = @"_zoneName";
NSString * const RecordIdsListParamKey = @"_recordIds";
NSString * const ZoneIdsListParamKey = @"_zoneIds";
NSString * const RecordChangeTagParamKey = @"_recordChangeTag";

NSString * const RecordsToSaveParamKey = @"_recordsToSave";
NSString * const RecordIdsToDeleteParamKey = @"_recordIdsToDelete";
NSString * const SavedRecordsParamKey = @"_savedRecords";
NSString * const DeletedRecordIDsParamKey = @"_deletedRecordIDs";
NSString * const SavePolicyParamKey = @"_savePolicy";
NSString * const IsAtomicParamKey = @"_isAtomic";

NSString * const ZonesToSaveParamKey = @"_zonesToSave";
NSString * const ZoneNamesToDeleteParamKey = @"_zoneNamesToDelete";
NSString * const SavedZonesParamKey = @"_savedZones";
NSString * const DeletedZoneNamesParamKey = @"_deletedZoneNames";

NSString * const ErrorParamKey = @"_error";
NSString * const ErrorCodeParamKey = @"_code";
NSString * const ErrorMessageParamKey = @"_message";

NSString * const AccountStatusParamKey = @"_status";

NSString * const RecordIdNameParamKey = @"recordName";
NSString * const ZoneIdNameParamKey = @"zoneName";
