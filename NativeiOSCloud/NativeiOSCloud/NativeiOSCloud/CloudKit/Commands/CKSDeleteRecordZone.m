//
//  CKDeleteRecordZoneCommand.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/21/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSDeleteRecordZone.h"

@implementation CKSDeleteRecordZone

- (instancetype) initWithObject: (id) obj andStorage: (CloudKitStorage *) storage
{
    if (self = [super initWithObject: obj andStorage: storage])
    {
        [self initZoneId: obj];
    }
    
    return self;
}

- (void) initZoneId: (id) data
{
    NSString *zoneName = [data objectForKey: ZoneNameParamKey];
    _zoneId = [[CKRecordZoneID alloc] initWithZoneName: zoneName ownerName: CKCurrentUserDefaultName];
}

- (void) execute
{
    __weak typeof(self) weakSelf = self;
    
    [self.database deleteRecordZoneWithID: self.zoneId completionHandler: ^(CKRecordZoneID *zoneID, NSError *error)
    {
        typeof(self) strongSelf = weakSelf;
        if (!strongSelf) return;
        
        NSMutableDictionary *result = [NSMutableDictionary dictionary];
        BOOL isSuccess = YES;
        
        if (error)
        {
            NSDictionary *errorDict = [CKSUnitySerializer serializeError: error];
            [result setObject: errorDict forKey: ErrorParamKey];
            isSuccess = NO;
        }
        
        if (zoneID)
        {
            [result setObject: zoneID.zoneName forKey: ZoneNameParamKey];
        }
        
        [result setObject: [NSNumber numberWithBool: isSuccess] forKey: IsSuccessParamKey];
        [strongSelf processResponse: strongSelf.requestId data: result];
    }];
}

- (void) sendResult: (const char *) requestId responseData: (const char *) responseData
{
    unityCallbacks.HandleDeleteRecordZone(requestId, responseData);
}

@end
