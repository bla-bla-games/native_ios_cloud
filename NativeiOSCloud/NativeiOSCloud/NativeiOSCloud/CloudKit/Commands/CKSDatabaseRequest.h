//
//  CKSDatabaseRequest.h
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 6/14/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSRequest.h"

@interface CKSDatabaseRequest : CKSRequest

@property (nonatomic, readonly, strong) CKDatabase *database;

@end
