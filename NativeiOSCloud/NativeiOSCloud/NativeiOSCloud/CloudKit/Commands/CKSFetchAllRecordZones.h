//
//  CKFetchAllRecordZonesCommand.h
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/21/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSRequest.h"
#import "CKSDatabaseRequest.h"

@interface CKSFetchAllRecordZones : CKSDatabaseRequest

@end
