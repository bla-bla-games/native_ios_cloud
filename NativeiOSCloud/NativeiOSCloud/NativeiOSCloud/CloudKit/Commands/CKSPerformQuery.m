//
//  CKPerformQueryCommand.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/24/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSPerformQuery.h"

@implementation CKSPerformQuery

- (instancetype) initWithObject: (id) obj andStorage: (CloudKitStorage *) storage
{
    if (self = [super initWithObject: obj andStorage: storage])
    {
        id queryData = [obj objectForKey: QueryParamKey];
        [self initQuery: queryData];
    }
    
    return self;
}

- (void) initZoneId: (id) data
{
    NSString *zoneName = (NSString *)[data objectForKey: ZoneNameParamKey];
    _zoneId = [[CKRecordZoneID alloc] initWithZoneName: zoneName ownerName: CKCurrentUserDefaultName];
}

- (void) initQuery: (id) data
{
    [self initZoneId: data];
    
    NSString *recordType = [data objectForKey: RecordTypeParamKey];
    id predicateData = [data objectForKey: PredicateParamKey];
    NSPredicate *predicate = [self getPredicate: predicateData];
    
    _query = [[CKQuery alloc] initWithRecordType: recordType predicate: predicate];
}

- (NSPredicate *) getPredicate: (NSString *) format
{
    NSPredicate *result = nil;
    
    if (!format || [format length] == 0)
    {
        result = [NSPredicate predicateWithValue: YES];
        return result;
    }
    
    result = [NSPredicate predicateWithFormat: format];
    
    return result;
}

- (void) execute
{
    __weak typeof(self) weakSelf = self;
    
    [self.database performQuery: self.query
                   inZoneWithID: self.zoneId
              completionHandler: ^(NSArray<CKRecord *> *results, NSError *error)
    {
        typeof(self) strongSelf = weakSelf;
        if (!strongSelf) return;
        
        NSMutableDictionary *result = [NSMutableDictionary dictionary];
        BOOL isSuccess = YES;
        
        if (error)
        {
            NSDictionary *errorDict = [CKSUnitySerializer serializeError: error];
            [result setObject: errorDict forKey: ErrorParamKey];
            isSuccess = NO;
        }
        
        if (results)
        {
            NSMutableArray *recordsList = [NSMutableArray array];

            for (CKRecord *record in results)
            {
                NSDictionary *recordDict = [CKSUnitySerializer serializeRecord: record];
                [recordsList addObject: recordDict];
            }
            
            [result setObject: recordsList forKey: RecordsListParamKey];
        }
        
        [result setObject: [NSNumber numberWithBool: isSuccess] forKey: IsSuccessParamKey];
        [strongSelf processResponse: strongSelf.requestId data: result];
    }];
}

- (void) sendResult: (const char *) requestId responseData: (const char *) responseData
{
    unityCallbacks.HandlePerformQuery(requestId, responseData);
}

@end
