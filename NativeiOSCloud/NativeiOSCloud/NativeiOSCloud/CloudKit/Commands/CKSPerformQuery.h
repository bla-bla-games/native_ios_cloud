//
//  CKPerformQueryCommand.h
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/24/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSRequest.h"
#import "CKSDatabaseRequest.h"

@interface CKSPerformQuery : CKSDatabaseRequest

@property (nonatomic, readonly, strong) CKRecordZoneID *zoneId;
@property (nonatomic, readonly, strong) CKQuery *query;

@end
