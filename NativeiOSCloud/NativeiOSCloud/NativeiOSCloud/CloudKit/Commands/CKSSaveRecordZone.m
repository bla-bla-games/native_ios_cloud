//
//  SaveZoneCommand.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/20/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSSaveRecordZone.h"

@implementation CKSSaveRecordZone

- (instancetype) initWithObject: (id) obj andStorage: (CloudKitStorage *) storage
{
    if (self = [super initWithObject: obj andStorage: storage])
    {
        id zoneData = [obj objectForKey: ZoneParamKey];
        [self initZone: zoneData];
    }
    
    return self;
}

- (void) initZone: (id) data
{
    NSString *zoneName = (NSString *)[data objectForKey: ZoneNameParamKey];
    _zone = [[CKRecordZone alloc] initWithZoneName: zoneName];
}

- (void) execute
{
    __weak typeof(self) weakSelf = self;
    
    [self.database saveRecordZone: _zone completionHandler: ^(CKRecordZone *zone, NSError *error)
    {
        typeof(self) strongSelf = weakSelf;
        if (!strongSelf) return;
        
        NSMutableDictionary *result = [NSMutableDictionary dictionary];
        BOOL isSuccess = YES;
        
        if (error)
        {
            NSDictionary *errorDict = [CKSUnitySerializer serializeError: error];
            [result setObject: errorDict forKey: ErrorParamKey];
            isSuccess = NO;
        }
        
        if (zone)
        {
            NSDictionary *zoneDict = [CKSUnitySerializer serializeZone: zone];
            [result setObject: zoneDict forKey: ZoneParamKey];
        }
        
        [result setObject: [NSNumber numberWithBool: isSuccess] forKey: IsSuccessParamKey];
        [strongSelf processResponse: strongSelf.requestId data: result];
    }];
}

- (void) sendResult: (const char *) requestId responseData: (const char *) responseData
{
    unityCallbacks.HandleSaveRecordZone(requestId, responseData);
}

@end
