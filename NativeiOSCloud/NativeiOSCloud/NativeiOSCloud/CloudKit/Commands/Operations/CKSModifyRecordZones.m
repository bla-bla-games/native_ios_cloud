//
//  CKSModifyRecordZones.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 6/9/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSModifyRecordZones.h"

@implementation CKSModifyRecordZones

- (instancetype) initWithObject: (id) obj andStorage: (CloudKitStorage *) storage
{
    if (self = [super initWithObject: obj andStorage: storage])
    {
        [self initOperation: obj];
    }
    
    return self;
}

- (void) initZonesToSave: (NSArray *) data
{
    _zonesToSave = [NSMutableArray array];
    
    for (id zoneData in data)
    {
        CKRecordZone *zone = [CKSUnitySerializer deserializeZone: zoneData];
        [_zonesToSave addObject: zone];
    }
}

- (void) initZoneIDsToDelete: (NSArray *) data
{
    _zoneIDsToDelete = [NSMutableArray array];
    
    for (NSString *zoneName in data)
    {
        CKRecordZoneID *recordID = [CKSUnitySerializer deserializeZoneID: zoneName];
        [_zoneIDsToDelete addObject: recordID];
    }
}

- (void) initOperation: (id) data
{
    NSArray *zonesToSaveData = (NSArray *)[data objectForKey: ZonesToSaveParamKey];
    NSArray *zoneNamesToDeleteData = (NSArray *)[data objectForKey: ZoneNamesToDeleteParamKey];
    
    [self initZonesToSave: zonesToSaveData];
    [self initZoneIDsToDelete: zoneNamesToDeleteData];
    
    __weak typeof(self) weakSelf = self;
    
    _operation = [[CKModifyRecordZonesOperation alloc] initWithRecordZonesToSave: self.zonesToSave recordZoneIDsToDelete: self.zoneIDsToDelete];

    self.operation.modifyRecordZonesCompletionBlock = ^(NSArray<CKRecordZone *> *savedRecordZones, NSArray<CKRecordZoneID *> *deletedRecordZoneIDs, NSError *operationError)
    {
        typeof(self) strongSelf = weakSelf;
        if (!strongSelf) return;
        
        NSMutableDictionary *result = [NSMutableDictionary dictionary];
        BOOL isSuccess = YES;
        
        if (operationError)
        {
            NSDictionary *errorDict = [CKSUnitySerializer serializeError: operationError];
            [result setObject: errorDict forKey: ErrorParamKey];
            isSuccess = NO;
        }
        
        // gather saved zones
        NSMutableArray *savedZonesData = [NSMutableArray array];
        for (CKRecordZone *zone in savedRecordZones)
        {
            id serializedZone = [CKSUnitySerializer serializeZone: zone];
            [savedZonesData addObject: serializedZone];
        }
        //
        
        // gather deleted zone ids
        NSMutableArray *deletedZoneIDsData = [NSMutableArray array];
        for (CKRecordZoneID *zoneID in deletedRecordZoneIDs)
        {
            id serializedZoneID = [CKSUnitySerializer serializeZoneID: zoneID];
            [deletedZoneIDsData addObject: serializedZoneID];
        }
        //
        
        [result setObject: savedZonesData forKey: SavedZonesParamKey];
        [result setObject: deletedZoneIDsData forKey: DeletedZoneNamesParamKey];
        [result setObject: [NSNumber numberWithBool: isSuccess] forKey: IsSuccessParamKey];
        
        [strongSelf processResponse: strongSelf.requestId data: result];
    };
}

- (void) execute
{
    [self.database addOperation: self.operation];
}

- (void) sendResult: (const char *) requestId responseData: (const char *) responseData
{
    unityCallbacks.HandleModifyRecordZones(requestId, responseData);
}

@end
