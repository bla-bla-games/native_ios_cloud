//
//  CKSModifyRecords.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 6/9/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSModifyRecords.h"

@implementation CKSModifyRecords

- (instancetype) initWithObject: (id) obj andStorage: (CloudKitStorage *) storage
{
    if (self = [super initWithObject: obj andStorage: storage])
    {
        _recordIdsWithError = [NSMutableArray array];
        [self initOperation: obj];
    }
    
    return self;
}

- (void) initRecordsToSave: (NSArray *) data
{
    _recordsToSave = [NSMutableArray array];
    
    for (id recordData in data)
    {
        CKRecord *record = [CKSUnitySerializer deserializeRecord: recordData];
        [_recordsToSave addObject: record];
    }
}

- (void) initRecordIDsToDelete: (NSArray *) data
{
    _recordIDsToDelete = [NSMutableArray array];
    
    for (id recordIdData in data)
    {
        CKRecordID *recordID = [CKSUnitySerializer deserializeRecordID: recordIdData];
        [_recordIDsToDelete addObject: recordID];
    }
}

- (void) initProperties: (id) data
{
    _savePolicy = [[data objectForKey: SavePolicyParamKey] integerValue];
    _isAtomic = [[data objectForKey: IsAtomicParamKey] boolValue];
}

- (void) initOperation: (id) data
{
    NSArray *recordsToSaveData = (NSArray *)[data objectForKey: RecordsToSaveParamKey];
    NSArray *recordIDsToDeleteData = (NSArray *)[data objectForKey: RecordIdsToDeleteParamKey];
    
    [self initRecordsToSave: recordsToSaveData];
    [self initRecordIDsToDelete: recordIDsToDeleteData];
    [self initProperties: data];
    
    __weak typeof(self) weakSelf = self;
    
    _operation = [[CKModifyRecordsOperation alloc] initWithRecordsToSave: self.recordsToSave recordIDsToDelete: self.recordIDsToDelete];
    _operation.savePolicy = self.savePolicy;
    _operation.atomic = self.isAtomic;
    
    self.operation.perRecordCompletionBlock = ^(CKRecord *record, NSError *error)
    {
        typeof(self) strongSelf = weakSelf;
        if (!strongSelf) return;
        
        if (error)
        {
            NSDictionary *recordErrorDict = [CKSUnitySerializer serializeRecordError: record.recordID error: error];
            [strongSelf.recordIdsWithError addObject: recordErrorDict];
        }
    };
    
    self.operation.modifyRecordsCompletionBlock = ^(NSArray<CKRecord *> *savedRecords, NSArray<CKRecordID *> *deletedRecordIDs, NSError *operationError)
    {
        typeof(self) strongSelf = weakSelf;
        if (!strongSelf) return;
        
        NSMutableDictionary *result = [NSMutableDictionary dictionary];
        BOOL isSuccess = YES;
        
        if (operationError)
        {
            NSDictionary *errorDict = [CKSUnitySerializer serializeError: operationError];
            [result setObject: errorDict forKey: ErrorParamKey];
            isSuccess = NO;
        }
        
        // gather saved records
        NSMutableArray *savedRecordsData = [NSMutableArray array];
        for (CKRecord *record in savedRecords)
        {
            id serializedRecord = [CKSUnitySerializer serializeRecord: record];
            [savedRecordsData addObject: serializedRecord];
        }
        //
        
        // gather deleted record ids
        NSMutableArray *deletedRecordIDsData = [NSMutableArray array];
        for (CKRecordID *recordID in deletedRecordIDs)
        {
            id serializedRecordID = [CKSUnitySerializer serializeRecordID: recordID];
            [deletedRecordIDsData addObject: serializedRecordID];
        }
        //
        
        [result setObject: savedRecordsData forKey: SavedRecordsParamKey];
        [result setObject: deletedRecordIDsData forKey: DeletedRecordIDsParamKey];
        [result setObject: strongSelf.recordIdsWithError forKey: RecordsErrorsParamKey];
        [result setObject: [NSNumber numberWithBool: isSuccess] forKey: IsSuccessParamKey];
        
        [strongSelf processResponse: strongSelf.requestId data: result];
    };
}

- (void) execute
{
    [self.database addOperation: self.operation];
}

- (void) sendResult: (const char *) requestId responseData: (const char *) responseData
{
    unityCallbacks.HandleModifyRecords(requestId, responseData);
}

@end
