//
//  CKFetchRecordsCommand.h
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/24/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSRequest.h"
#import "CKSDatabaseRequest.h"

@interface CKSFetchMultipleRecords : CKSDatabaseRequest

@property (nonatomic, readonly, strong) CKFetchRecordsOperation *operation;
@property (nonatomic, readonly, strong) NSMutableArray<CKRecordID *> *recordIDs;
@property (nonatomic, readonly, strong) NSMutableArray *recordIdsWithError;

@end
