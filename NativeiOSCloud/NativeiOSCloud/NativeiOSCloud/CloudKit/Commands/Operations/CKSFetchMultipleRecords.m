//
//  CKFetchRecordsCommand.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/24/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSFetchMultipleRecords.h"

@implementation CKSFetchMultipleRecords

- (instancetype) initWithObject: (id) obj andStorage: (CloudKitStorage *) storage
{
    if (self = [super initWithObject: obj andStorage: storage])
    {
        _recordIdsWithError = [NSMutableArray array];
        [self initOperation: obj];
    }
    
    return self;
}

- (void) initRecordIds: (NSArray *) data
{
    _recordIDs = [NSMutableArray array];
    
    for (id recordIDData in data)
    {
        CKRecordID *recordID = [CKSUnitySerializer deserializeRecordID: recordIDData];
        [self.recordIDs addObject: recordID];
    }
}

- (void) initOperation: (id) data
{
    NSArray *rawRecords = (NSArray *)[data objectForKey: RecordIdsListParamKey];
    [self initRecordIds: rawRecords];
    
    __weak typeof(self) weakSelf = self;
    
    _operation = [[CKFetchRecordsOperation alloc] initWithRecordIDs: self.recordIDs];
    
    self.operation.perRecordCompletionBlock = ^(CKRecord *record, CKRecordID *recordID, NSError *error)
    {
        typeof(self) strongSelf = weakSelf;
        if (!strongSelf) return;
        
        if (error)
        {
            NSDictionary *recordErrorDict = [CKSUnitySerializer serializeRecordError: recordID error: error];
            [strongSelf.recordIdsWithError addObject: recordErrorDict];
        }
    };
    
    self.operation.fetchRecordsCompletionBlock = ^(NSDictionary<CKRecordID *, CKRecord *> *recordsByRecordID, NSError *operationError)
    {
        typeof(self) strongSelf = weakSelf;
        if (!strongSelf) return;
        
        NSMutableDictionary *result = [NSMutableDictionary dictionary];
        BOOL isSuccess = YES;
        
        if (operationError)
        {
            NSDictionary *errorDict = [CKSUnitySerializer serializeError: operationError];
            [result setObject: errorDict forKey: ErrorParamKey];
            isSuccess = NO;
        }
        
        if (recordsByRecordID)
        {
            NSMutableArray *recordsList = [NSMutableArray array];
            
            for (CKRecordID *recordId in recordsByRecordID)
            {
                CKRecord *record = [recordsByRecordID objectForKey: recordId];
                NSDictionary *recordDict = [CKSUnitySerializer serializeRecord: record];
                [recordsList addObject: recordDict];
            }
            
            [result setObject: recordsList forKey: RecordsListParamKey];
        }

        [result setObject: strongSelf.recordIdsWithError forKey: RecordsErrorsParamKey];
        [result setObject: [NSNumber numberWithBool: isSuccess] forKey: IsSuccessParamKey];
        [strongSelf processResponse: strongSelf.requestId data: result];
    };
}

- (void) execute
{
    [self.database addOperation: self.operation];
}

- (void) sendResult: (const char *) requestId responseData: (const char *) responseData
{
    unityCallbacks.HandleFetchMultipleRecords(requestId, responseData);
}

@end
