//
//  CKSFetchMultipleRecordZones.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 6/8/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSFetchMultipleRecordZones.h"

@implementation CKSFetchMultipleRecordZones

- (instancetype) initWithObject: (id) obj andStorage: (CloudKitStorage *) storage
{
    if (self = [super initWithObject: obj andStorage: storage])
    {
        [self initOperation: obj];
    }
    
    return self;
}

- (void) initOperation: (id) data
{
    NSArray *rawZoneIds = (NSArray *)[data objectForKey: ZoneIdsListParamKey];
    NSMutableArray<CKRecordZoneID *> *zoneIds = [NSMutableArray array];
    
    __weak typeof(self) weakSelf = self;
    
    for (NSString *zoneName in rawZoneIds)
    {
        CKRecordZoneID *zoneId = [[CKRecordZoneID alloc] initWithZoneName: zoneName ownerName: CKCurrentUserDefaultName];
        [zoneIds addObject: zoneId];
    }
    
    _operation = [[CKFetchRecordZonesOperation alloc] initWithRecordZoneIDs: zoneIds];
    
    self.operation.fetchRecordZonesCompletionBlock = ^(NSDictionary<CKRecordZoneID *,CKRecordZone *> *recordZonesByZoneID, NSError *operationError)
    {
        typeof(self) strongSelf = weakSelf;
        if (!strongSelf) return;
        
        NSMutableDictionary *result = [NSMutableDictionary dictionary];
        BOOL isSuccess = YES;
        
        if (operationError)
        {
            NSDictionary *errorDict = [CKSUnitySerializer serializeError: operationError];
            [result setObject: errorDict forKey: ErrorParamKey];
            isSuccess = NO;
        }
        
        if (recordZonesByZoneID)
        {
            NSMutableArray *zonesList = [NSMutableArray array];
            
            for (CKRecordZoneID *zoneId in recordZonesByZoneID)
            {
                CKRecordZone *zone = [recordZonesByZoneID objectForKey: zoneId];
                NSDictionary *zoneDict = [CKSUnitySerializer serializeZone: zone];
                [zonesList addObject: zoneDict];
            }
            
            [result setObject: zonesList forKey: ZonesListParamKey];
        }
        
        [result setObject: [NSNumber numberWithBool: isSuccess] forKey: IsSuccessParamKey];
        [strongSelf processResponse: strongSelf.requestId data: result];
    };
}

- (void) execute
{
    [self.database addOperation: self.operation];
}

- (void) sendResult: (const char *) requestId responseData: (const char *) responseData
{
    unityCallbacks.HandleFetchMultipleRecordZones(requestId, responseData);
}

@end
