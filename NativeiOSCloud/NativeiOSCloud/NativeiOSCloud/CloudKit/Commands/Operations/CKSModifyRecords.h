//
//  CKSModifyRecords.h
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 6/9/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSRequest.h"
#import "CKSDatabaseRequest.h"

@interface CKSModifyRecords : CKSDatabaseRequest

@property (nonatomic, readonly, strong) CKModifyRecordsOperation *operation;
@property (nonatomic, readonly, strong) NSMutableArray<CKRecord *> *recordsToSave;
@property (nonatomic, readonly, strong) NSMutableArray<CKRecordID *> *recordIDsToDelete;
@property (nonatomic, readonly, strong) NSMutableArray *recordIdsWithError;
@property (nonatomic, readonly, assign) CKRecordSavePolicy savePolicy;
@property (nonatomic, readonly, assign) BOOL isAtomic;

@end
