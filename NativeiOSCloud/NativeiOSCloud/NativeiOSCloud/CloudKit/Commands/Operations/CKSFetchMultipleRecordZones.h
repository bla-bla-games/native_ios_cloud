//
//  CKSFetchMultipleRecordZones.h
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 6/8/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSRequest.h"
#import "CKSDatabaseRequest.h"

@interface CKSFetchMultipleRecordZones : CKSDatabaseRequest

@property (nonatomic, readonly, strong) CKFetchRecordZonesOperation *operation;

@end
