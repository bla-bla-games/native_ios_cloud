//
//  CKSModifyRecordZones.h
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 6/9/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSRequest.h"
#import "CKSDatabaseRequest.h"

@interface CKSModifyRecordZones : CKSDatabaseRequest

@property (nonatomic, readonly, strong) CKModifyRecordZonesOperation *operation;
@property (nonatomic, readonly, strong) NSMutableArray<CKRecordZone *> *zonesToSave;
@property (nonatomic, readonly, strong) NSMutableArray<CKRecordZoneID *> *zoneIDsToDelete;

@end
