//
//  CKSaveCommand.h
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/17/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CloudKit/CloudKit.h>
#import "CKSRequest.h"
#import "CKSDatabaseRequest.h"

@interface CKSSaveRecord : CKSDatabaseRequest

@property (nonatomic, readonly, strong) CKRecord *record;

@end
