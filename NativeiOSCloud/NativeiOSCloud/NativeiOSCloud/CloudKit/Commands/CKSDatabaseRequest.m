//
//  CKSDatabaseRequest.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 6/14/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSDatabaseRequest.h"

@implementation CKSDatabaseRequest

- (instancetype) initWithObject: (id) obj andStorage: (CloudKitStorage *) storage
{
    if (self = [super initWithObject: obj andStorage: storage])
    {
        id databaseData = [obj objectForKey: DatabaseParamKey];
        [self initDatabase: databaseData];
    }
    
    return self;
}

- (void) initDatabase: (id) data
{
    NSString *containerId = [data objectForKey: ContainerIdParamKey];
    NSInteger databaseScope = [[data objectForKey: DatabaseScopeParamKey] integerValue];
    
    CKContainer *container = [self.storage getContainer: containerId];
    _database = [self.storage getDatabase: container withDBScope: databaseScope];
}

@end
