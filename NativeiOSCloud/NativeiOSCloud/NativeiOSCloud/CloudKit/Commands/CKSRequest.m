//
//  CKCommand.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/19/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSRequest.h"
#import <CloudKit/CloudKit.h>
#import <Utilities.m>
#import <KSJSON.h>

@implementation CKSRequest

- (instancetype) initWithObject: (id) obj andStorage: (CloudKitStorage *) storage
{
    if (self = [super init])
    {
        _storage = storage;
        [self initRequestId: obj];
    }
    
    return self;
}

- (void) initRequestId: (id) data
{
    _requestId = [data objectForKey: RequestIdParamKey];
}

- (void) execute
{
}

- (void) processResponse: (NSString *) requestId data: (NSDictionary *) responseData
{
    [self callInMainThread: ^{
        
        const char *c_requestId = fromNSString(requestId);
        
        NSString *serializedResponseData = [KSJSON serializeObject: responseData error: nil];
        const char *c_serializedResponseData = fromNSString(serializedResponseData);
        
        [self sendResult: c_requestId responseData: c_serializedResponseData];
        [self notifyAboudFinishing];
        
    }];
}

- (void) sendResult: (const char *) requestId responseData: (const char *) responseData
{
}

- (void) notifyAboudFinishing
{
    if (self.delegate)
    {
        [self.delegate requestDidFinish: self];
    }
}

- (void) callInMainThread: (void (^)(void)) blockToCall
{
    if ([NSThread isMainThread])
    {
        blockToCall();
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            blockToCall();
        });
    }
}

- (void) finalize
{
}

@end
