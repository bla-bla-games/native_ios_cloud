//
//  CKSContainerRequest.h
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 6/14/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSRequest.h"

@interface CKSContainerRequest : CKSRequest

@property (nonatomic, readonly, strong) CKContainer *container;

@end
