//
//  CloudKitUnityCallbacks.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/28/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (*CKS_Unity_AccountStatusHandler)(const char *, const char *);

typedef void (*CKS_Unity_PerformQueryHandler)(const char *, const char *);

typedef void (*CKS_Unity_SaveRecordHandler)(const char *, const char *);
typedef void (*CKS_Unity_DeleteRecordHandler)(const char *, const char *);
typedef void (*CKS_Unity_FetchRecordHandler)(const char *, const char *);

typedef void (*CKS_Unity_SaveRecordZoneHandler)(const char *, const char *);
typedef void (*CKS_Unity_DeleteRecordZoneHandler)(const char *, const char *);
typedef void (*CKS_Unity_FetchRecordZoneHandler)(const char *, const char *);
typedef void (*CKS_Unity_FetchAllRecordZonesHandler)(const char *, const char *);

typedef void (*CKS_Unity_FetchMultipleRecordsHandler)(const char *, const char *);
typedef void (*CKS_Unity_FetchMultipleRecordZonesHandler)(const char *, const char *);
typedef void (*CKS_Unity_ModifyRecordsHandler)(const char *, const char *);
typedef void (*CKS_Unity_ModifyRecordZonesHandler)(const char *, const char *);

struct CloudKitUnityCallbacks
{
    CKS_Unity_AccountStatusHandler HandleAccountStatus;
    
    CKS_Unity_PerformQueryHandler HandlePerformQuery;
    
    CKS_Unity_SaveRecordHandler HandleSaveRecord;
    CKS_Unity_DeleteRecordHandler HandleDeleteRecord;
    CKS_Unity_FetchRecordHandler HandleFetchRecord;
    
    CKS_Unity_SaveRecordZoneHandler HandleSaveRecordZone;
    CKS_Unity_DeleteRecordZoneHandler HandleDeleteRecordZone;
    CKS_Unity_FetchRecordZoneHandler HandleFetchRecordZone;
    CKS_Unity_FetchAllRecordZonesHandler HandleFetchAllRecordZones;
    
    CKS_Unity_FetchMultipleRecordsHandler HandleFetchMultipleRecords;
    CKS_Unity_FetchMultipleRecordZonesHandler HandleFetchMultipleRecordZones;
    CKS_Unity_ModifyRecordsHandler HandleModifyRecords;
    CKS_Unity_ModifyRecordZonesHandler HandleModifyRecordZones;
} unityCallbacks;
