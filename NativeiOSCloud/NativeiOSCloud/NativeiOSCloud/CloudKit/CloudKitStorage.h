//
//  CloudKitStorage.h
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/15/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CloudKit/CloudKit.h>
#import "CKSRequestDelegate.h"

@interface CloudKitStorage : NSObject<CKSRequestDelegate>

@property (nonatomic, readonly, strong) NSMutableDictionary * _Nonnull activeRequests;

- (void) activate;
- (void) deactivate;

#pragma mark Account Status
- (void) accountStatus: (NSString *_Nonnull) requestData;

#pragma mark Query
- (void) performQuery: (NSString *_Nonnull) requestData;

#pragma mark Records
- (void) saveRecord: (NSString *_Nonnull) requestData;
- (void) deleteRecord: (NSString *_Nonnull) requestData;
- (void) fetchRecord: (NSString *_Nonnull) requestData;

#pragma mark Zones
- (void) saveRecordZone: (NSString *_Nonnull) requestData;
- (void) deleteRecordZone: (NSString *_Nonnull) requestData;
- (void) fetchRecordZone: (NSString *_Nonnull) requestData;
- (void) fetchAllRecordZones: (NSString *_Nonnull) requestData;

#pragma mark Operations
- (void) fetchMultipleRecords: (NSString *_Nonnull) requestData;
- (void) fetchMultipleRecordZones: (NSString *_Nonnull) requestData;
- (void) modifyRecords: (NSString *_Nonnull) requestData;
- (void) modifyRecordZones: (NSString *_Nonnull) requestData;

#pragma mark Utils
- (CKContainer *_Nullable) getContainer: (NSString *_Nullable) containerId;
- (CKDatabase *_Nullable) getDatabase: (CKContainer *_Nonnull) container withDBScope: (CKDatabaseScope) scope;

@end
