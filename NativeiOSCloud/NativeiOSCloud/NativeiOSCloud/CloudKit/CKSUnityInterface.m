//
//  CloudKitUnityInterface.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/28/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CKSUnityCallbacks.m"
#import "NativeiOSCloud.h"
#import <Utilities.m>

void _native_activate(CKS_Unity_AccountStatusHandler accountStatusCB,
                      
                      CKS_Unity_PerformQueryHandler performQueryCB,
                      
                      CKS_Unity_SaveRecordHandler saveRecordCB,
                      CKS_Unity_DeleteRecordHandler deleteRecordCB,
                      CKS_Unity_FetchRecordHandler fetchRecordCB,
                      
                      CKS_Unity_SaveRecordZoneHandler saveRecordZoneCB,
                      CKS_Unity_DeleteRecordZoneHandler deleteRecordZoneCB,
                      CKS_Unity_FetchRecordZoneHandler fetchRecordZoneCB,
                      CKS_Unity_FetchAllRecordZonesHandler fetchAllRecordZonesCB,
                      
                      CKS_Unity_FetchMultipleRecordsHandler fetchMultipleRecordsCB,
                      CKS_Unity_FetchMultipleRecordZonesHandler fetchMultipleRecordZonesCB,
                      CKS_Unity_ModifyRecordsHandler modifyRecordsCB,
                      CKS_Unity_ModifyRecordZonesHandler modifyRecordZonesCB)
{
    // account status
    unityCallbacks.HandleAccountStatus = accountStatusCB;
    
    // query
    unityCallbacks.HandlePerformQuery = performQueryCB;
    
    // record
    unityCallbacks.HandleSaveRecord = saveRecordCB;
    unityCallbacks.HandleDeleteRecord = deleteRecordCB;
    unityCallbacks.HandleFetchRecord = fetchRecordCB;
    
    // zone
    unityCallbacks.HandleSaveRecordZone = saveRecordZoneCB;
    unityCallbacks.HandleDeleteRecordZone = deleteRecordZoneCB;
    unityCallbacks.HandleFetchRecordZone = fetchRecordZoneCB;
    unityCallbacks.HandleFetchAllRecordZones = fetchAllRecordZonesCB;
    
    // operation
    unityCallbacks.HandleFetchMultipleRecords = fetchMultipleRecordsCB;
    unityCallbacks.HandleFetchMultipleRecordZones = fetchMultipleRecordZonesCB;
    unityCallbacks.HandleModifyRecords = modifyRecordsCB;
    unityCallbacks.HandleModifyRecordZones = modifyRecordZonesCB;
    
    [[NativeiOSCloud sharedInstance].cloudKit activate];
}

void _native_deactivate()
{
    // account status
    unityCallbacks.HandleAccountStatus = NULL;
    
    // query
    unityCallbacks.HandlePerformQuery = NULL;
    
    // record
    unityCallbacks.HandleSaveRecord = NULL;
    unityCallbacks.HandleDeleteRecord = NULL;
    unityCallbacks.HandleFetchRecord = NULL;
    
    // zone
    unityCallbacks.HandleSaveRecordZone = NULL;
    unityCallbacks.HandleDeleteRecordZone = NULL;
    unityCallbacks.HandleFetchRecordZone = NULL;
    unityCallbacks.HandleFetchAllRecordZones = NULL;
    
    // operation
    unityCallbacks.HandleFetchMultipleRecords = NULL;
    unityCallbacks.HandleFetchMultipleRecordZones = NULL;
    unityCallbacks.HandleModifyRecords = NULL;
    unityCallbacks.HandleModifyRecordZones = NULL;
    
    [[NativeiOSCloud sharedInstance].cloudKit deactivate];
}

#pragma mark Account Status

void _native_accountStatus(const char *requestData)
{
    NSString *data = toNSString(requestData);
    [[NativeiOSCloud sharedInstance].cloudKit accountStatus: data];
}

#pragma mark Query Interface

void _native_performQuery(const char *requestData)
{
    NSString *data = toNSString(requestData);
    [[NativeiOSCloud sharedInstance].cloudKit performQuery: data];
}

#pragma mark Records Interface

void _native_saveRecord(const char *requestData)
{
    NSString *data = toNSString(requestData);
    [[NativeiOSCloud sharedInstance].cloudKit saveRecord: data];
}

void _native_deleteRecord(const char *requestData)
{
    NSString *data = toNSString(requestData);
    [[NativeiOSCloud sharedInstance].cloudKit deleteRecord: data];
}

void _native_fetchRecord(const char *requestData)
{
    NSString *data = toNSString(requestData);
    [[NativeiOSCloud sharedInstance].cloudKit fetchRecord: data];
}

#pragma mark Zones Interface

void _native_saveRecordZone(const char *requestData)
{
    NSString *data = toNSString(requestData);
    [[NativeiOSCloud sharedInstance].cloudKit saveRecordZone: data];
}

void _native_deleteRecordZone(const char *requestData)
{
    NSString *data = toNSString(requestData);
    [[NativeiOSCloud sharedInstance].cloudKit deleteRecordZone: data];
}

void _native_fetchRecordZone(const char *requestData)
{
    NSString *data = toNSString(requestData);
    [[NativeiOSCloud sharedInstance].cloudKit fetchRecordZone: data];
}

void _native_fetchAllRecordZones(const char *requestData)
{
    NSString *data = toNSString(requestData);
    [[NativeiOSCloud sharedInstance].cloudKit fetchAllRecordZones: data];
}

#pragma mark Operations Interface

void _native_fetchMultipleRecords(const char *requestData)
{
    NSString *data = toNSString(requestData);
    [[NativeiOSCloud sharedInstance].cloudKit fetchMultipleRecords: data];
}

void _native_fetchMultipleRecordZones(const char *requestData)
{
    NSString *data = toNSString(requestData);
    [[NativeiOSCloud sharedInstance].cloudKit fetchMultipleRecordZones: data];
}

void _native_modifyRecords(const char *requestData)
{
    NSString *data = toNSString(requestData);
    [[NativeiOSCloud sharedInstance].cloudKit modifyRecords: data];
}

void _native_modifyRecordZones(const char *requestData)
{
    NSString *data = toNSString(requestData);
    [[NativeiOSCloud sharedInstance].cloudKit modifyRecordZones: data];
}
