//
//  CloudKitUnitySerializer.m
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 5/28/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import "CKSUnitySerializer.h"
#import "CKSRequestParamKeys.h"

@implementation CKSUnitySerializer

NSString * const DEFAULT_ZONE_ID = @"_defaultZone";

+ (id) serializeRecord: (CKRecord *) record
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    [result setObject: [CKSUnitySerializer serializeRecordID: record.recordID] forKey: RecordIdParamKey];
    [result setObject: record.recordType forKey: RecordTypeParamKey];
    [result setObject: record.recordChangeTag forKey: RecordChangeTagParamKey];
    
    NSMutableArray *customFields = [NSMutableArray array];
    [result setObject: customFields forKey: RecordFieldsListParamKey];
    
    for (NSString *key in record.allKeys)
    {
        NSMutableDictionary *field = [NSMutableDictionary dictionary];
        [field setObject: key forKey: RecordFieldKeyParamKey];
        [field setObject: record[key] forKey: RecordFieldValueParamKey];
        [customFields addObject: field];
    }
    
    return result;
}

+ (CKRecord *) deserializeRecord: (id) recordData
{
    id recordIdData = [recordData objectForKey: RecordIdParamKey];
    
    NSString *recordName = [recordIdData objectForKey: RecordNameParamKey];
    NSString *zoneName = [recordIdData objectForKey: ZoneNameParamKey];
    NSString *type = [recordData objectForKey: RecordTypeParamKey];
    
    CKRecord *result = [[CKRecord alloc] initWithRecordType: type];
    
    zoneName = [self parseZoneName: zoneName];
    
    if (zoneName)
    {
        [result.recordID.zoneID setValue: zoneName forKey: ZoneIdNameParamKey];
    }
    
    [result.recordID setValue: recordName forKey: RecordIdNameParamKey];
    
    id customFields = [recordData objectForKey: RecordFieldsListParamKey];
    
    for (id fieldData in customFields)
    {
        NSString *fieldKey = [fieldData objectForKey: RecordFieldKeyParamKey];
        NSString *fieldValue = [fieldData objectForKey: RecordFieldValueParamKey];
        
        result[fieldKey] = fieldValue;
    }
    
    return result;
}

+ (id) serializeRecordID: (CKRecordID *) recordID
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    [result setObject: recordID.recordName forKey: RecordNameParamKey];
    [result setObject: recordID.zoneID.zoneName forKey: ZoneNameParamKey];
    return result;
}

+ (CKRecordID *) deserializeRecordID: (id) recordIDData
{
    NSString *recordName = [recordIDData objectForKey: RecordNameParamKey];
    NSString *zoneName = [recordIDData objectForKey: ZoneNameParamKey];
    zoneName = [self parseZoneName: zoneName];
    
    CKRecordID *result = [[CKRecordID alloc] initWithRecordName: recordName];
    
    if (zoneName)
    {
        [result.zoneID setValue: zoneName forKey: ZoneIdNameParamKey];
    }
    
    return result;
}

+ (id) serializeZone: (CKRecordZone *) zone
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    [result setObject: zone.zoneID.zoneName forKey: ZoneNameParamKey];
    [result setObject: zone.zoneID.ownerName forKey: ZoneOwnerParamKey];
    
    return result;
}

+ (CKRecordZone *) deserializeZone: (id) zoneData
{
    NSString *zoneName = [zoneData objectForKey: ZoneNameParamKey];
    zoneName = [self parseZoneName: zoneName];
    
    CKRecordZone *result;
    
    if (zoneName)
    {
         result = [[CKRecordZone alloc] initWithZoneName: zoneName];
    }
    else
    {
        result = [[CKRecordZone alloc] initWithZoneName: CKRecordZoneDefaultName];
    }
    
    return result;
}

+ (id) serializeZoneID: (CKRecordZoneID *) zoneID
{
    return zoneID.zoneName;
}

+ (CKRecordZoneID *) deserializeZoneID: (id) zoneIDData
{
    NSString *zoneName = (NSString *)zoneIDData;
    zoneName = [self parseZoneName: zoneName];
    
    CKRecordZoneID *result;
    
    if (zoneName)
    {
        result = [[CKRecordZoneID alloc] initWithZoneName: zoneName ownerName: CKCurrentUserDefaultName];
    }
    else
    {
        result = [[CKRecordZoneID alloc] initWithZoneName: CKRecordZoneDefaultName ownerName: CKCurrentUserDefaultName];
    }
    
    return result;
}

+ (id) serializeError: (NSError *) error
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    [result setObject: [NSNumber numberWithInteger: error.code] forKey: ErrorCodeParamKey];
    [result setObject: error.localizedDescription forKey: ErrorMessageParamKey];
    return result;
}

+ (id) serializeRecordError: (CKRecordID *) recordId error: (NSError *) error
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    [result setObject: [NSNumber numberWithInteger: error.code] forKey: ErrorCodeParamKey];
    [result setObject: error.localizedDescription forKey: ErrorMessageParamKey];
    [result setObject: [self serializeRecordID: recordId] forKey: RecordIdParamKey];
    return result;
}

+ (id) serializeRecordZoneError: (NSString *) zoneName error: (NSError *) error
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    [result setObject: [NSNumber numberWithInteger: error.code] forKey: ErrorCodeParamKey];
    [result setObject: error.localizedDescription forKey: ErrorMessageParamKey];
    [result setObject: zoneName forKey: ZoneNameParamKey];
    return result;
}

+ (NSString *) parseZoneName: (NSString *) zoneName
{
    if (!zoneName || [zoneName isEqualToString: DEFAULT_ZONE_ID])
    {
        return CKRecordZoneDefaultName;
    }
    
    return zoneName;
}

+ (NSString *) getZoneName: (CKRecordZoneID *) zoneId
{
    BOOL isDefaultZoneId = [self isDefaultZoneId: zoneId];
    
    if (isDefaultZoneId)
    {
        return DEFAULT_ZONE_ID;
    }
    
    return zoneId.zoneName;
}

+ (BOOL) isDefaultZoneId: (CKRecordZoneID *) zoneId
{
    CKRecordZoneID *defaultZoneId = [CKRecordZone defaultRecordZone].zoneID;
    
    if ([defaultZoneId.zoneName isEqualToString: zoneId.zoneName])
    {
        return NO;
    }
    
    return YES;
}

@end
