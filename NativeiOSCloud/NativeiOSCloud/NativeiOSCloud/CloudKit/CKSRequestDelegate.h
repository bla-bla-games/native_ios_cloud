//
//  CKSRequestDelegate.h
//  NativeiOSCloud
//
//  Created by Darkwing Duck on 6/3/17.
//  Copyright © 2017 Bla Bla Games. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CKSRequest;

@protocol CKSRequestDelegate <NSObject>

- (void) requestDidFinish: (CKSRequest *) request;

@end
